﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security;
using MySql.Data.MySqlClient;

namespace BackupMonitor {
    public partial class frmPrincipal : Form {
        public frmPrincipal() {
            InitializeComponent();
        }

        private void btnFolder_Click(object sender, EventArgs e) {
            folderBrowserDialog1.ShowDialog();

            if (folderBrowserDialog1.SelectedPath != "") {
                txtDestino.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void saveConfig() {
            string fileTXT = AppDomain.CurrentDomain.BaseDirectory+"\\config.txt";

            try {
                StreamWriter sw = new StreamWriter(fileTXT, false);
                sw.WriteLine(txtDestino.Text);
                sw.WriteLine(cbmDiaBackup.SelectedIndex);
                sw.WriteLine(txtHoraBackup.Text);
                sw.WriteLine(chkCompactar.Checked);

                sw.WriteLine(txtServer.Text);
                sw.WriteLine(txtPass.Text);
                sw.WriteLine(txtPorta.Text);
                sw.WriteLine(txtUser.Text);
                sw.WriteLine(comboDB.SelectedIndex);

                sw.Close();

                MessageBox.Show("Configurações salvas com sucesso!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            } catch (IOException e) {
                MessageBox.Show("Ocorreu um erro ao salvar configurações! " + e.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            } 
        }

        private void loadConfig() { 
            string fileTXT = AppDomain.CurrentDomain.BaseDirectory+"\\config.txt";

            if(File.Exists(fileTXT)) {
                try {
                    string[] linhas = File.ReadAllLines(fileTXT, Encoding.UTF8);

                    txtDestino.Text            = linhas[0];
                    cbmDiaBackup.SelectedIndex = Int32.Parse(linhas[1]);
                    txtHoraBackup.Text         = linhas[2];
                    chkCompactar.Checked       = linhas[3] == "True" ? true : false;
                    txtServer.Text             = linhas[4];
                    txtPass.Text               = linhas[5];
                    txtPorta.Text              = linhas[6];
                    txtUser.Text               = linhas[7];
                   // comboDB.SelectedIndex      = Int32.Parse(linhas[8]);

                } catch (IOException e) {
                    MessageBox.Show("Ocorreu um erro ao recuperar configurações! " + e.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                 MessageBox.Show("Arquivo de configurações Config.txt não existe! ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); 
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            saveConfig();
        }

        private void button2_Click(object sender, EventArgs e) {
            listBox1.Items.Add("Iniciando: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            Database db = new Database();

            MySqlConnection conn = db.connectDatabase(txtServer.Text, comboDB.SelectedIndex.ToString(), txtUser.Text, txtPass.Text, false);

            listBox1.Items.Add("Abrindo conexão com banco: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            conn.Open();

            listBox1.Items.Add("Exportando banco de dados " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            if (db.dumpDatabase(txtDestino.Text)) {
                listBox1.Items.Add("Backup finalizado com sucesso!: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                MessageBox.Show("Backup finalizado com sucesso!! ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else {
                listBox1.Items.Add("Ocorreu um erro ao fazer backup: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                MessageBox.Show("Ocorreu um erro ao fazer backup! ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            while (db.listDatabases().Read()) { 
                Console.WriteLine(db.listDatabases()["Database"]);
                comboDB.Items.Add(db.listDatabases()["Database"]);
            } 
        }

        private void txtTestConnection_Click(object sender, EventArgs e) {
            Database db = new Database();

            try {
                MySqlConnection conn = db.connectDatabase(txtServer.Text, comboDB.SelectedIndex.ToString(), txtUser.Text, txtPass.Text, true);

                conn.Open();
                conn.Close();

            } catch (Exception ex) {
                MessageBox.Show("Erro ao conectar no banco! " + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void frmPrincipal_Load(object sender, EventArgs e) {
            loadConfig();
        }
    }
}
