﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;

namespace BackupMonitor {
    class Database {
        private string server = null;
        private string db     = null;
        private string user   = null;
        private string pwd    = null;
        private bool teste    = false;

        public MySqlConnection connectDatabase(string server, string db, string user, string pwd, bool teste = false) {

            this.server = server;
            this.db     = db;
            this.user   = user;
            this.pwd    = pwd;
            this.teste  = teste;
            
            string strCon = "Server=" + this.server + ";Database=" + this.db + ";Uid=" + this.user + ";Pwd=" + this.pwd;

            try {
                MySqlConnection conn = new MySqlConnection(strCon);

                if (teste) {
                    MessageBox.Show("Conectado ao banco! ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return conn;
            } catch (MySqlException e) {

                MessageBox.Show("Erro ao conectar com banco de dados! " + e.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return null;
            }
        }

        public MySqlDataReader listDatabases() {
 
            MySqlConnection conn = this.connectDatabase(this.server, this.db, this.user, this.pwd, false);
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "SHOW DATABASES;";

            try {
                conn.Open();
                MySqlDataReader reader = command.ExecuteReader();

                return reader;
            } catch (Exception e) {
                MessageBox.Show("Erro! " + e.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            } 
        }

        public bool dumpDatabase(string strDestino = "") {
            bool retorno = false;

            strDestino = strDestino +  @"\" + this.db+".sql";

            string arguments = $" -e -f -h {this.server} -u {this.user} -p{this.pwd} {this.db} --opt --routines --triggers --result-file={strDestino}";

            try {
                Process p = new Process();
                p.StartInfo.WorkingDirectory = @"C:\Program Files\MySQL\MySQL Server 5.6\bin\";
                p.StartInfo.FileName = "mysqldump";
                p.StartInfo.Arguments = arguments;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.Start();

                p.WaitForExit();
                p.Close();

                retorno = true;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                retorno = false;
            }

            return retorno;
        }
    }
}
